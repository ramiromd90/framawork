<?php
if(FW_DEVELOPMENT)
{
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
}

/**
 * Registro los autoloades.
 * NO MODIFICAR EL ORDEN !!
 */
Twig_Autoloader::register();
spl_autoload_register('fw_autoload');

/**
 * Framawork autoloader.
 * 
 * @param  string $ns Namespace de la clase a instanciar.
 * @return void
 */
function fw_autoload($ns) {
	$path = FW_SITE_PATH . "/" . str_replace("\\", "/", $ns) . ".php";
	include_once($path);
}


$registry = new App\Core\Registry();


$twig_loader = new Twig_Loader_Filesystem(FW_TEMPLATES);
$twig_cfg = array('cache' => FW_TEMPLATE_CACHE);
$registry->view = new \Twig_Environment($twig_loader, $twig_cfg);
$registry->db = new App\Core\Database();

$router = new App\Core\Router($registry);
$router->routing($registry);
?>