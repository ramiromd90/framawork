<?php
namespace App\Core;
use \PDO as PDO;

class Database{
	const DSN = "%s:host=%s;dbname=%s";
	const ERROR = "<b>Framawork cant connect to database, because %s.</b>";
	private $db;

	public function __construct(){
		$this->db = $this->doConnection();
	}

	/**
	 * Devuelve una instancia del almacen solicitado
	 * por parametro.
	 * @param  string $aStore El nombre de una clase almacen.
	 * @return App\Core\Store La instancia de un almacen concreto.
	 */
	public function getStore($aStore){
		$normalized = "Src\Store\\" . ucfirst($aStore);
		return new $normalized($this->db);
	}

	/**
	 * Realiza la conexion a la base de datos.
	 * @return \PDO Una instancia de PDO.
	 */
	private function doConnection(){
		$aConfig = $this->getConfiguration();
		$dsn = sprintf(self::DSN, $aConfig->driver, $aConfig->host, $aConfig->dbname);

		try{
			$gdb = new PDO($dsn, $aConfig->user, $aConfig->password);
			return $gdb;
		}catch(PDOException $e){
			die(sprintf(self::ERROR, $e->getMessage()));
		}
	}

	/**
	 * Parsea el archivo de configuración de la bbdd.
	 * @return StdClass()
	 */
	private function getConfiguration(){
		$env = (FW_DEVELOPMENT) ? "dev" : "prod";
		$config_json = file_get_contents(FW_SITE_PATH . "/App/Config/database_$env.json");
		$config = json_decode($config_json);		
		return $config->database;
	}

	public function generateModels(){
		$mg = new App\Core\Generator\Model($this->db);
		$mg->generateModels();
	}

	public function generateModel($table){
		$mg = new Generator\Model($this->db);
		$mg->generateModel($table);
	}

}