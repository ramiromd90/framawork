<?php
namespace App\Core\Generator;
use \PDO as PDO;
use \Exception as Exception;

/**
 * Utilidad que permite generar los modelos de datos
 * a partir de una instancia de PDO.
 *
 * @author Ramiro Martínez D'Elía ramiro.md90@gmail.com
 */
class Model{

	/**
	 * Constantes útiles para el reemplazo de bloques dentro del
	 * template de modelos.
	 */
	const PROPERTY_TPL = "/**\n\t * @var %s \n\t **/\n\tprotected $%s;\n\n\t";
	const GETTER_TPL = "public function get%s(){\n\t\treturn %s;\n\t}\n\n\t";
	const SETTER_TPL = "public function set%s(%s){\n\t\t%s = %s;\n\t}\n\n\t";

	/**
	 * Otras constantes útiles.
	 */
	const TABLE_NOT_FOUND = "\033[31mEl esquema %s no existe.\n\033[0m";
	const PRINT_ON = "Esquema: \033[32m %s \033[0m volcado en: \033[32m %s \033[0m\n";

	/**
	 * Contiene la instancia de PDO asociada.
	 * @var PDO
	 */
	private $db;

	/**
	 * Contiene el contenido del archivo
	 * plantilla de modelos.
	 * 
	 * @var string
	 */
	private $template;

	/**
	 * Contiene la lista de tablas para el
	 * esquema asociado a $this->db.
	 * 
	 * @var mixed[]
	 */
	private $tables;

	/**
	 * Contiene el nombre de la tabla que
	 * esta siendo procesada actualmente
	 * por la instancia.
	 * 
	 * @var string
	 */
	private $table;

	/**
	 * Contiene el nombre del modelo que esta
	 * siendo procesado actualmente por la 
	 * instancia.
	 * 
	 * @var string
	 */
	private $name;

	/**
	 * @param \PDO $db Una conexion a la bbdd a
	 * consultar.
	 */
	public function __construct($db){
		$this->db = $db;

		// Obtengo las tablas del esquema a modelar
		$sth = $this->db->query("SHOW TABLES");
		$tables = $sth->fetchAll(PDO::FETCH_NUM);
		$this->tables = Array();

		// Aplano el arreglo que devolvió la query
		foreach($tables as $table){
			$this->tables[] = $table[0];
		}
	}

	/**
	 * Genera el modelo de una tabla especifica
	 * del esquema.
	 * 
	 * @param  string $table El nombre de una tabla
	 * @throws Exception Si la tabla no existe.
	 * @return void
	 */
	public function generateModel($table){

		if(!in_array($table, $this->tables)){
			$message = sprintf(self::TABLE_NOT_FOUND, $table);
			throw new Exception($message);
		}

		// Cargo el template
		$tpl_path = FW_SITE_PATH . "/App/Core/Generator/model.template";
		$this->template = file_get_contents($tpl_path);

		$this->table = $table;
		$this->loadTableColumns();

		// Empiezo a escribir la clase
		$created_at = date("Y-m-d H:i:s");
		$this->template = str_replace("%creation_date%", $created_at, $this->template);
		$this->name = $this->toPascalCase($this->table);
		$this->template = str_replace("%name%", $this->name, $this->template);

		// Escribo las properties
		$properties = "";
		foreach($this->columns as $column){
			$properties .= sprintf(self::PROPERTY_TPL, $column->Type, $column->Field);
		}
		$this->template = str_replace("%properties%", $properties, $this->template);

		// Escribo los accessors
		$accessors = "";
		foreach($this->columns as $column){
			$__this = "$" . "this->" . $column->Field;
			$__val = "$" . "value";
			$accessor_name = $this->toPascalCase($column->Field);
			$accessors .= sprintf(self::GETTER_TPL, $accessor_name, $__this);
			$accessors .= sprintf(self::SETTER_TPL, $accessor_name, $__val, $__this, $__val);
		}
		$this->template = str_replace("%accessors%", $accessors, $this->template);

		// Persisto la clase a disco
		$class_rel_path = "Src/Model/" . $this->name . ".php";
		$class_full_path = FW_SITE_PATH . "/" . $class_rel_path;
		$file = fopen($class_full_path, "w+");
		fwrite($file, $this->template);
		fclose($file);

		$this->generateStore();

		// Informo en pantalla :)
		$message = sprintf(self::PRINT_ON, $this->table, $class_rel_path);
		print($message);
	}

	/**
	 * Genera todos los modelos del esquema
	 * asociado a la conexion PDO.
	 * 
	 * @return void
	 */
	public function generateModels(){
		foreach($this->tables as $table){
			$this->generateModel($table);
		}
	}

	/**
	 * Recupera la informacion de las columnas para la
	 * tabla actual del esquema.
	 * 
	 * @return void
	 */
	private function loadTableColumns(){
		$sth = $this->db->query("SHOW COLUMNS FROM " . $this->table);
		$this->columns = $sth->fetchAll(PDO::FETCH_CLASS, 'StdClass');
	}

	/**
	 * Convierte una cadena de texto a Pascal Case.
	 * @param string El texto de entrada.
	 * @return string El texto pasado a Pascal Case.
	 */
	private function toPascalCase($string){
		$normalized = "";

		if(stripos($string, "_")){
			$segments = explode("_", $string);
			foreach($segments as $segment){
				$normalized .= ucfirst($segment);
			}
		}else{
			$normalized = ucfirst($string);
		}
			
		return $normalized;
	}

	private function generateStore(){
		// Cargo el template
		$tpl_path = FW_SITE_PATH . "/App/Core/Generator/store.template";
		$this->template = file_get_contents($tpl_path);
		$created_at = date("Y-m-d H:i:s");
		$this->template = str_replace("%creation_date%", $created_at, $this->template);
		$this->template = str_replace("%name%", $this->name, $this->template);

		// Persisto la clase a disco
		$class_rel_path = "Src/Store/" . $this->name . ".php";
		$class_full_path = FW_SITE_PATH . "/" . $class_rel_path;
		$file = fopen($class_full_path, "w+");
		fwrite($file, $this->template);
		fclose($file);
	}

}