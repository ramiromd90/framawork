<?php

/* Defino la constante con la ruta raiz del proyecto. */
define("FW_SITE_PATH", realpath(dirname(__FILE__)));

/**
 * Incluyo todas las librerias y configuraciones
 * basicas para la inicializacion de Framawork
 */
include("App/Config/Constants.php");
include("App/Core/Utils.php");
include("App/Core/HttpErrorHandler.php");
include("App/Third/Twig/Autoloader.php");
include("App/Bootstrap.php");