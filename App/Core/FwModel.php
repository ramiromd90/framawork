<?php

namespace App\Core;

abstract class FwModel{

	public function getVars(){
		return get_object_vars($this);
	}
}