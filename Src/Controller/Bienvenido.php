<?php

namespace Src\Controller;
use App\Core\FwController as FwController;
use Src\Model\Pet as Pet;

class Bienvenido extends FwController{

	/**
	 * Default Action
	 * @return void
	 */
	public function index(){
		echo "Bienvenido a Framawork";
	}

	public function goodbye($user){
		$view_params = array('user' => $user);
		$this->render('Demo/Goodbye.html.twig', $view_params);
	}

	public function hello($user){
		$view_params = array('user' => $user);
		$this->render('Demo/Hello.html.twig', $view_params);
	}
}