<?php
namespace App\Core;
use \PDO as PDO;
use \Exception as Exception;

abstract class FwStore{

	/**
	 * Contiene la instancia de PDO asociada.
	 * @var PDO
	 */
	protected $db;
	/**
	 * Nombre del esquema representado en la bbdd.
	 * @var string
	 */
	protected $table;
	/**
	 * Nombre de la clase modelo a utilizar.
	 * @var string
	 */
	protected $model;

	/**
	 * Contiene el último error del PDOStatment.
	 * @var  mixed[]
	 */
	protected $last_error;

	/**
	 * @param PDO $db Instancia PDO
	 */
	public function __construct(PDO $db){
		$this->db = $db;
		$this->table = $this->resolveTableName();
		$this->model = "Src\Model\\" . ucfirst($this->table);
	}

	/**
	 * Obtiene a partir del nombre de la clase,
	 * el nombre de la tabla en la bbdd.
	 * 
	 * @return string Nombre de la tabla
	 */
	protected function resolveTableName(){
		$namespace = get_called_class();
		$segments = explode("\\", $namespace);
		$class_name = lcfirst(end($segments));
		$table_name = "";

		$chars = str_split($class_name);
		foreach($chars as $char){
			if(ctype_upper($char)){
				$table_name .= "_" . strtolower($char);
			}else{
				$table_name .= $char;
			}
		}

		return $table_name;
	}

	/**
	 * Informa si existe algun registro con el id
	 * especificado, en el almacén de datos.
	 * 
	 * @param  int $id El id del registro.
	 * @return boolean
	 */
	public function anySatisfy($id){
		$sql = "SELECT COUNT(*) FROM {$this->table} WHERE id = ?";

		$sth = $this->db->prepare($sql);
		$params = Array($id);

		if($sth->execute($params) && ($sth->fetchColumn() == 1)){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * Devuelve un modelo que se corresponda con el id
	 * pasado por parámetro. O bien, null, si no hay
	 * asociaciones.
	 * 
	 * @param  int $id Id del registro a buscar
 	 * @return App\Core\Model
	 */
	public function find($id){
		if($this->anySatisfy($id)){
			$sth = $this->db->prepare("SELECT * FROM {$this->table} WHERE id = ?");
			$params = Array($id);
			$sth->execute($params);

			// Manejador de errores
			if($sth->errorCode() != 0){
				$errors = $sth->errorInfo();
				throw new Exception($errors[2]);
			}

			$result = $sth->fetchAll(PDO::FETCH_CLASS, $this->model);
			return $result[0];
		}else{
			return null;
		}
	}

	/**
	 * Recupera todos los modelos para la tabla
	 * actual.
	 * 
	 * @return Model[]
	 */
	public function findAll(){
		$sth = $this->db->prepare("SELECT * FROM {$this->table} WHERE 1");
		$sth->execute();

		return $sth->fetchAll(PDO::FETCH_CLASS, $this->model);
	}

	/**
	 * Devuelve la cantidad de registros de la tabla.
	 * 
	 * @return int Total de registros
	 */
	public function count(){
		$sth = $this->db->prepare("SELECT COUNT(*) FROM {$this->table}");	
		$sth->execute();

		return (int) $sth->fetchColumn();
	}

	/**
	 * Elimina un modelo de la bbdd.
	 * 
	 * @param  App\Core\Model $aModel El modelo a elminar
	 * @return boolean El estado de la operación
	 */
	public function delete($aModel){
		$sth = $this->db->prepare("DELETE FROM {$this->table} WHERE id = ?");
		$params = Array($aModel->getId());
		$result = $sth->execute($params);
		
		// Si es 0, me lo toma como FALSE.
		return $result;
	}

	/**
	 * Persiste un modelo en la bbdd.
	 * 
	 * @param  App\Model\BaseModel $aModel El modelo a persistir
	 * @return boolean El estado de la operación 
	 */
	public function save($aModel){
		if($this->anySatisfy($aModel->getId())){
			return $this->update($aModel);
		}else{
			return $this->create($aModel);
		}
	}

	/**
	 * Persiste un modelo nuevo en la bbdd.
	 * 
	 * @param  App\Model\BaseModel $aModel El modelo a persistir
	 * @return boolean El estado de la operación 
	 */
	protected function create($aModel){
		$vars = $aModel->getVars();
		$paramColumns = implode(',', array_fill(0, count($vars), '?'));
		$paramValues = array_values($vars);
		
		$sql = "INSERT INTO {$this->table} VALUES ({$paramColumns})";
		$sth = $this->db->prepare($sql);
		$result = $sth->execute($paramValues);

		if($result){
			$aModel->setId($this->db->lastInsertId());
		}

		return $result;
	}

	/**
	 * Persiste un modelo editado en la bbdd.
	 * 
	 * @param  App\Model\BaseModel $aModel El modelo a persistir
	 * @return boolean El estado de la operación 
	 */
	protected function update($aModel){
		$vars = $aModel->getVars();
		//$id = __array_pop($vars, 'id');
		$set_clausure = "";
		$keys = array_keys($vars);
		foreach($keys as $key){
			$set_clausure .=  $key . "= ?,";
		}
		$set_clausure = substr($set_clausure, 0, -1);
		$paramValues = array_values($vars);
		$paramValues[] = $paramValues[0];

		

		$sql = "UPDATE {$this->table} SET {$set_clausure} WHERE id = ?";
		$sth = $this->db->prepare($sql);
		$result = $sth->execute($paramValues);

		return $result;
	}
}