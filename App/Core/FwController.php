<?php

namespace App\Core;

/**
 * Controlador abstracto, con la funcionalidad minima.
 *
 * Todos los controladores definidos por el usuario deben extender
 * de FwController. Esta es la definicion basica de todo controlador.
 */

abstract class FwController{

	protected $view;

	public function __construct($registry){
		$this->view = $registry->view;
		$this->db = $registry->db;
	}
	/**
	 * Todos los controladores deben implementar
	 * este metodo, ya que es la accion default
	 * configurada en Framawork.
	 */
	abstract function index();

	/**
	 * Renderiza una vista.
	 * @param  string $template La ruta relativa a la plantilla.
	 * @param  mixed[] $params  Los parametros a enviar a la plantilla.
	 */
	protected function render($template, $params){
		echo $this->view->render($template, $params);
	}

}