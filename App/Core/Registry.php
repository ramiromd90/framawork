<?php
namespace App\Core;
/**
 * Almacena todos las varibles compartidas por la app, siendo una alternativa
 * a las variables globales.
 * Cualquier controlador puede acceder a este objeto registro.
 */

class Registry{

	private $vars;

	public function __construct(){
		$this->vars = Array();
	}

	public function __set($index, $value){
		$this->vars[$index] = $value;
	}

	public function __get($index){
		return $this->vars[$index];
	}
}