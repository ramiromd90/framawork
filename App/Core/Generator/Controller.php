<?php
namespace App\Core\Generator;
use \PDO as PDO;
use \Exception as Exception;

/**
 * Utilidad que permite generar los controladores.
 *
 * @author Ramiro Martínez D'Elía ramiro.md90@gmail.com
 */
class Controller{

	public static function generate($name){

		$tpl_path = FW_SITE_PATH . "/App/Core/Generator/controller.template";
		$template = file_get_contents($tpl_path);


		$controller_name = ucfirst($name);
		$template = str_replace("%name%", $controller_name, $template);

		$creation_at = date("Y-m-d H:i:s");
		$template = str_replace("%creation_date%", $creation_at, $template);		

		// Persisto la clase a disco
		$class_rel_path = "/Src/Controller/" . $controller_name . ".php";
		$class_full_path = FW_SITE_PATH . "/" . $class_rel_path;
		$file = fopen($class_full_path, "w+");
		fwrite($file, $template);
		fclose($file);
	}

}