<?php
/**
 * Definicion de constantes utilizadas por Framawork,
 * editables por el usuario.
 */
define("FW_DEFAULT_CONTROLLER", "Bienvenido");
define("FW_DEFAULT_ACTION", "index");
define("FW_FILE_NOT_FOUND", "El archivo `%s` no existe ");
define("FW_INVALID_ACTION", "El metodo `%s` no existe ");
define("FW_INVALID_ARGS", "Argumentos incorrectos para `%s` ");
define("FW_DEVELOPMENT", true);
define("FW_TEMPLATES", FW_SITE_PATH . "/Src/View");
define("FW_TEMPLATE_CACHE", FW_SITE_PATH . "/App/Cache");


/**
 * Definicion de constantes utilizadas por Framawork,
 * NO editables por el usuario.
 */
