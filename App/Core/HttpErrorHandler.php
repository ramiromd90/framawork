<?php

namespace App\Core;

/**
 * Clase controladora de errores HTTP.
 *
 * Configura las cabeceras y muestra la pagina correspondiente
 * segun el error.
 */

class HttpErrorHandler{

	/**
	 * Maneja el lanzamiento de un error 404.
	 */
	static public function pageNotFound(){
		header("HTTP/1.0 404 Not Found - Archive Empty");
		$template = FW_SITE_PATH . "/App/View/404.php";
		include($template);
		die();
	}
	
	/**
	 * Maneja el lanzamiento de un error 500.
	 */
	static public function internalServerError(){
		header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
		$template = FW_SITE_PATH . "/App/View/505.php";
		include($template);
		die();
	}
}