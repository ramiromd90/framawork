<?php

/**
 * Debugea una variable y corta la ejecucion
 * del script.
 * @param mixed $var La variable a inspeccionar.
 */
function var_die($var){
	var_dump($var);
	die();
}

/**
 * URL actual.
 * @return string
 */
function get_url(){
	$protocol = $_SERVER['SERVER_PROTOCOL'];
	$protocol = substr($protocol, 0, strpos($protocol, "/"));
	return strtolower($protocol) . "://" . $_SERVER['SERVER_NAME'] .  $_SERVER['REQUEST_URI'];
}

/**
 * Lanza un error PHP, utilizado en entorno de
 * desarrollo.
 * @param  string $message El mensaje de error.
 */
function throw_error($message){
	trigger_error($message);
	die();
}

/**
 * Devuelve el dominio del proyecto.
 * @return string
 */
function base_url(){
	return "http://" . $_SERVER['SERVER_NAME'];
}